<!--
title: Modélisation de la consommation
description: 
published: true
date: 2025-01-28T15:40:14.296Z
tags: 
editor: ckeditor
dateCreated: 2021-05-18T15:55:32.881Z
-->

<h1>Introduction</h1>
<p>La modélisation de la consommation dans <strong>express'eau</strong> offre une grand souplesse d'utilisation.&nbsp;</p>
<p>Une couche des consommations géolocalisées peut être importée dans le modèle sans allocation préalable aux nœuds ou aux canalisations ce qui permet une facilité d'utilisation et une <u>traçabilité de la donnée</u>.</p>
<p>Par ailleurs, la notion de secteurs de consommation et de scénarios de consommation permet, à partir des données de base des consommations, de créer de nouveaux scénarios sans dégrader la donnée d'origine.&nbsp;</p>
<h1>Modélisation de la consommation et de la demande</h1>
<p>La consommation est modélisée dans Express'eau à plusieurs niveaux :</p>
<ul>
  <li>Dans les <u>points de consommation</u> (couche <i><strong>Water_delivery_point</strong></i>) : le volume journalier consommé est renseigné. Cette couche distingue les consommations dites “domestiques” et les consommations dites “industrielles” :<ul>
      <li>Les consommations domestiques appartiennent aux secteurs de consommation.</li>
      <li>Les consommations industrielles n'appartiennent pas aux secteurs de consommation e sont traités distinctement des consommations domestiques.</li>
    </ul>
  </li>
  <li>Dans <u>les nœuds</u> (couche <i><strong>User_node</strong></i>) : le volume journalier de chaque point de consommation est alloué au nœud le plus proche. La courbe de modulation horaire est propre à chaque nœud pour les consommations domestiques.</li>
  <li>Dans <u>les secteurs de consommation</u> (couche <i><strong>Water_delivery_sector</strong></i>) &nbsp;: le rendement de référence de chaque secteur est renseigné.</li>
  <li>Dans <code>expresseau → Water delivery → Water delivery sectors settings</code> est présenté :<ul>
      <li>la liste des secteurs avec le volume domestique total (somme des consommations domestiques sur tous les nœuds du secteur)</li>
      <li>pour chaque secteur, les nœuds lui appartenant, &nbsp;le volume journalier des consommations domestiques de chaque nœud ainsi que la courbe de modulation horaire appliquée au nœud</li>
      <li>la liste des courbes de modulation horaire existantes</li>
    </ul>
  </li>
  <li>Dans <code>expresseau → Water delivery → Water delivery scénarios</code> sont présentés plusieurs scénarios :<ul>
      <li>Le scénario de consommation de référence (<i><strong>WD REF SCN</strong></i>) : Ce scénario de consommation (qui n'est pas modifiable) présente pour chaque secteur :<ul>
          <li><u>le volume domestique total </u>qui est la somme des volumes domestiques allouées aux nœuds à partir des points de consommation),</li>
          <li><u>un coefficient journalier</u> égal à 1,</li>
          <li><u>le rendement de référence</u> (celui qui est renseigné dans la couche des secteurs de consommation).</li>
          <li><u>la liste des consommations industrielles</u> avec leur volume (celui qui est renseigné dans la table des points de consommations), le nœud d'allocation et la courbe de modulation horaires propre à chaque consommation industrielle.</li>
        </ul>
      </li>
      <li>Les autres scénarios de consommation qui présentent pour chaque secteur :<ul>
          <li><u>le volume domestique total</u> qui peut ici être modifié pour prendre en compte, par exemple, l'évolution de la population et des consommations.</li>
          <li><u>le coefficient journalier</u> qui peut ici être modifié pour modéliser par exemple un jour de pointe ou un jour de creux.</li>
          <li><u>le rendement </u>qui peut ici être modifié pour&nbsp;prendre en compte l'amélioration du rendement grâce aux travaux.</li>
          <li><u>la liste des consommations industrielles</u> avec leur volume qui peut être modifié, le nœud d'allocation et la courbe de modulation horaires propre à chaque consommation industrielle.</li>
        </ul>
      </li>
    </ul>
  </li>
</ul>
<blockquote>
  <p><strong>FOCUS :</strong> Le rendement de référence (celui utilisé dans le scénario de consommation de référence) est renseigné dans la table <i><strong>Water_delivery_sector </strong>alors que le rendement d'un scénario de consommation lambda est renseigné dans le paramétrage dudit scénario.</i></p>
</blockquote>
<h1>Ordre de construction du modèle</h1>
<ol>
  <li>Construire le modèle du réseau avec à minima les nœuds et les canalisations (couche<i><strong> User_node</strong> et <strong>Pipe_link</strong>)</i></li>
  <li>Importer ou tracer les secteurs de consommation (couche <i><strong>Water_delivery_sector</strong></i>)</li>
  <li>Importer ou créer les points de consommation (couche <i><strong>Water_delivery_point</strong>) : </i>au moment de la création, l'allocation aux nœuds via les canalisations se fait automatiquement.</li>
  <li>Dans <code>expresseau → Water delivery → Water delivery sectors settings</code> créer les courbes de modulation et les affecter aux nœuds.</li>
</ol>
<blockquote>
  <p><strong>ASTUCE :</strong> Pour une modification en série des courbes de modulation aux nœuds, préférez utiliser le tableau des attributs de la couche <i><strong>User_node</strong></i>. Vous pouvez par exemple sélectionner tous les nœuds d'un secteur et remplacer le nom de la courbe de modulation dans le tableau des attributs après l'avoir rendu modifiable. Si vous avez sélectionné un grand nombre de nœuds, le processus peut prendre plusieurs minutes.</p>
</blockquote>
<p>A ce stade, le scénario de consommation de référence (<i><strong>WD REF SCN</strong></i>) est prêt à être utilisé dans une simulation.</p>
<h1>Présentation détaillée</h1>
<h2>Les points de consommations</h2>
<p>Les points de consommation sont dans la table <i><strong>Water_delivery_point.</strong></i></p>
<p>Les champs à renseigner sont les suivants :</p>
<ul>
  <li><strong>Nom :</strong> nom unique</li>
  <li><strong>Volume</strong> (m3/j) : volume consommé</li>
  <li><strong>Industriel </strong>: <i>true/false</i> (ou case à cocher dans le fenêtre d'édition ou la table attributaire)</li>
  <li><strong>Source pipe link</strong> : identifiant de la canalisation sur laquelle allouer la consommation. Par défaut, c'est la canalisation la plus proche mais il est possible de la changer manuellement ou en cliquant sur la canalisation.</li>
</ul>
<p>Les autres champs (_<i>node et _affectation_geom </i>sont des champs liés à l'affectation qu'il est déconseillé de toucher).</p>
<figure class="image image_resized" style="width:54.4%;"><img src="/wd_points.png"></figure>
<blockquote>
  <p>Canalisation de transport ou de refoulement :</p>
  <p>Il existe une possibilité de définir des canalisations comme étant des canalisations sur lesquelles aucune consommation n'est directement raccordée. Pour cela, il faut passer le champs “is_feeder” à “True” (ou cocher la case “is_feeder”) dans la couche des canalisations, pour celles qui sont concernées.</p>
</blockquote>
<h2>Les secteurs de consommation</h2>
<p>Les secteurs de consommation sont dans la table <i><strong>Water_delivery_sector.</strong></i></p>
<figure class="image"><img src="/exemple_sectors.png"></figure>
<p>Les champs à renseigner sont les suivants :</p>
<ul>
  <li><strong>Nom :</strong> nom unique</li>
  <li><strong>Rendement</strong> (-) : rendement du secteur de distribution</li>
</ul>
<h2>Les nœuds</h2>
<p>Voir <a href="/modelisation/Conso_Node">User_Node</a></p>
<h2>Paramétrage des secteurs de consommation</h2>
<p>Le paramétrage des secteurs de consommations se fait via le menu <code>Express'Eau → Water Delivery → Water Delivery Sectors Settings.</code></p>
<p>Cette fenêtre se compose de deux parties.</p>
<p>La partie <code>Water delivery sectors</code> permet de visualiser et/ou modifier pour chaque secteur :</p>
<ul>
  <li>les nœuds appartenant au secteur,</li>
  <li>leurs volumes (domestique et industriel),</li>
  <li>leurs courbes de modulation (domestique et industrielle),</li>
  <li>le coefficient de contribution au sein du secteur (calculé automatiquement).</li>
</ul>
<figure class="image"><img src="/wd sectors setting haut.png"></figure>
<p>La partie <code>Hourly modulation curve </code>permet de créer et visualiser les courbes de modulation horaires.</p>
<figure class="image"><img src="/wd sectors setting bas.png"></figure>
<p>Les coefficients horaires doivent être préalablement estimés par l'utilisateur. Chaque valeur est définie comme le rapport de la consommation horaire de la tranche horaire sur la consommation horaire moyenne.</p>
<p>Par défaut, tous les coefficients sont égaux à 1, ce qui représente une consommation constante.</p>
<ul>
  <li>La courbe doit être composée de 24 valeurs.</li>
  <li>La somme des coefficients doit être égale à 24.</li>
</ul>
<p>Si ces contraintes ne sont pas respectées, la validation de la fenêtre de saisir mène à une erreur.</p>
<blockquote>
  <p>Un copier-coller dans le tableau des courbes est possible mais uniquement à partir d'un tableau de type Excel. Ne pas coller “Weekday” dans “Weekend” par exemple.</p>
</blockquote>
<h2>Paramétrage des scénarios de consommation</h2>
<p>Un scénario de consommation permet de définir la consommation journalière globale de chaque secteur et le rendement associé à chaque secteur.</p>
<h3>Le paramétrage d'un scénario de consommation</h3>
<p><code>Express'Eau → Water Delivery → Water Delivery&nbsp;Scenario</code></p>
<p>La fenêtre de paramétrage des scénarios de consommation permet d'afficher et de modifier chaque scénario de consommation avec pour chaque secteur :</p>
<ul>
  <li>Le volume global (m3/jour)</li>
  <li>Le coefficient d'ajustement (coefficient journalier par lequel sera multiplié le volume global)</li>
  <li>Le rendement (qui sert au moteur de calcul pour ajouter les pertes aux consommations)</li>
</ul>
<figure class="image"><img src="/scn conso manager.png"></figure>
<blockquote>
  <p><strong>NOTA sur le rendement</strong> : Attention à ce que le rendement renseigné permette de prendre en compte des volumes comptés mais non facturés, les volumes de services, etc. Ce n'est donc pas forcément le rendement affiché dans les RAD mais bien un coefficient qui doit permettre d'obtenir in fine le volume mis en distribution.</p>
  <p>Le calcul effectué par Express'Eau et le suivant :&nbsp;</p>
  <p>VMD= (1/r) * V<sub>CONSO</sub></p>
  <p>On a donc un V<sub>fuites</sub> = [(1/r) - 1] * V<sub>CONSO</sub></p>
  <p>où VMD : Volume Mis en Distribution (Volume consommé + fuites)</p>
</blockquote>
<p>Les données d’un <i><strong>scénario de consommation</strong></i> sont exploités par l’élément <i><strong>User_nod</strong></i> en mode de calcul «&nbsp;régime transitoire graduellement variable&nbsp;»&nbsp;: chaque point de consommation élémentaire <i><strong>User_nod</strong></i> est affecté à un <i><strong>secteur de consommation</strong></i>. Ces secteurs doivent être préalablement définis dans la table idoine. La mécanique de calcul de la consommation horaire d’un type donné (domestique ou industrielle) affectée à l’élément&nbsp; est la suivante&nbsp;:</p>
<ul>
  <li>Le programme calcule pour chaque élément <i><strong>User_nod</strong></i>&nbsp;<i> </i>la fraction de la consommation journalière du secteur prélevée par cet élément&nbsp;: Alpha_i = Ci/(somme(Ci).</li>
  <li>La consommation journalière prélevée par l’élément est égale à&nbsp;: Alpha_i x Conso_journalière.</li>
  <li>La courbe de modulation horaire définie dans le <i><strong>User_nod</strong></i>&nbsp; permet le calcul des consommations horaires à partir de la consommation journalière ci-dessus.</li>
</ul>
<p>Dans ce mode de calcul la consommation domestique (ou industrielle) affichée dans l’élément <i><strong>User_nod</strong></i>&nbsp; est interprété pour le calcul du paramètre Alpha_i qui est le paramètre de ventilation géographique de la consommation globale du secteur entre les différents points de consommation. Cette distribution est considérée comme relativement indépendante de la consommation globale&nbsp;: seuls les paramètres attachés au <i><strong>scénario de consommation </strong></i>doivent être modifiés , la consommation définie dans chaque élément <i><strong>User_nod</strong></i>&nbsp; n’a pas besoin d’être recalculée pour chaque nouvelle hypothèse de volume de consommation du secteur.</p>
<p><u>Cas où le&nbsp; </u><i><strong>User_nod</strong></i>&nbsp;<u> n'est pas dans un secteur de consommation :</u></p>
<p>Dans ce cas la consommation journalière attachée à un élément <i><strong>User_nod</strong></i>&nbsp; <mark class="pen-red"><u>n'est pas prise en compte</u></mark>.</p>
<p>Seul le Q0 du nœud est pris en compte puisqu'il est indépendant de la notion de secteur de consommation.&nbsp;</p>
<p>&nbsp;</p>
<h3>Le scénario de consommation de référence</h3>
<p>Le scénario de consommation de référence est un scénario de consommation particulier : le volume consommé par secteur est calculé et non modifiable. Il correspond à la somme des consommations aux nœuds appartenant à chaque secteur.</p>
<p>Dans ce cas, la consommation domestique (ou industrielle) affichée dans l’élément <i><strong>User_nod</strong></i> est bien la consommation qui sera soutirée à ce nœud.</p>
<h2>Paramétrage du scénario de calcul</h2>
<p>L'appel d'un scénario de consommation dans le gestionnaire de scénario se fait via l'onglet <code>Computation Settings</code> du gestionnaire de scénarios <code>Expresseau → Scénario → Settings</code>.</p>
<h1>La consommation lorsqu'il n'y a pas “assez” de pression</h1>
<p>Dans express'eau, le calcul des consommations est modifié s'il n'y a pas “assez” de pression sur le réseau. En effet, lorsque la pression à un nœud est inférieure à la pression minimale de service, le débit soutiré décroit linéairement avec la pression.</p>
<figure class="image"><img src="/decroissance_q_pmin.png"></figure>
<p>Cette pression minimale de service est fixée par défaut à 2 bars, soit 20mCE. Elle est modifiable dans la table de paramétrage “Threshold warnings” (les autres paramètres de cette couche sont inusités).</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
