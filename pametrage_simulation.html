<!--
title: Paramétrage d'une simulation
description: 
published: true
date: 2025-01-28T11:07:42.855Z
tags: 
editor: ckeditor
dateCreated: 2022-07-22T16:50:35.776Z
-->

<h1>Introduction</h1>
<p>Le paramétrage du scénario du scénario de fait via le menu <code>Express'eau → Scénarios → Paramétrage</code>.</p>
<p>La fenêtre de paramétrage présente plusieurs onglets:</p>
<ul>
  <li>Paramètrage des calculs</li>
  <li>Ordre des modèles</li>
  <li>Configurations et régulation</li>
  <li>Options de calcul</li>
  <li>Qualité de l'eau</li>
</ul>
<figure class="image image_resized" style="width:58.8%;"><img src="/gestionnaire_de_scenarios.png"></figure>
<h1>Paramétrage des calculs</h1>
<p>L’onglet est composé de plusieurs blocs&nbsp;:</p>
<ul>
  <li>Un bloc <strong>Mode de calcul </strong>(permanent, graduellement varié, rapidement varié qualité uniquement)&nbsp;:</li>
  <li>Un bloc de<strong> Paramétrage du calcul :</strong> de la date de début, de la durée et du pas de temps de la simulation (en secondes). Ce bloc contient aussi trois options complémentaires (affinage discrétisation, vaporisation et pas de sauvegarde pour le démarrage à chaud : <i>voir les paragraphes dédiés ci-dessous</i>).</li>
  <li>Un bloc de <strong>Choix du scénario de consommation</strong> et du coefficient journalier</li>
  <li>Un bloc <strong>Paramétrage des conditions initiales&nbsp;</strong>qui permet de paramétrer la durée de mise en eau du modèle ou d’effectuer un démarrage à chaud.</li>
  <li>Un bloc <strong>Paramétrage des&nbsp;sorties </strong>qui permet de paramétrer le pas de temps de sauvegarde des résultats (en secondes).</li>
  <li>Un bloc <strong>Scenario de référence</strong> qui permet de sélectionner le scenario de référence pour un gain de temps dans la phase de pre-processing.</li>
</ul>
<h2><strong>Mode de calcul</strong></h2>
<p>Express-Eau propose quatre modes distincts de calcul&nbsp;:&nbsp;</p>
<ol>
  <li><i>Permanent </i>: &nbsp;calcul en régime permanent</li>
  <li>Transitoire : calcul en régime transitoire évolutif, la compressibilité de l’eau est négligée.</li>
  <li>Transitoire rapide : calcul des régimes transitoires rapides, tenant compte de la compressibilité de l’eau et de l’élasticité des matériaux – simulation des coups de bélier.</li>
  <li>Défense extérieure contre les incendies : débit nominal</li>
  <li>Défense extérieure contre les incendies : débit à 1bar</li>
</ol>
<p>Ces modes de calcul sont détaillés sur la page dédiée : <a href="/calculs/modes">mode de calcul disponibles</a>.</p>
<h2><strong>Paramétrage du calcul&nbsp;</strong></h2>
<p>Ce bloc permet de paramétrer la date de début, &nbsp;la durée et le pas de temps de la simulation (en secondes).&nbsp;</p>
<p>Ce bloc contient aussi trois options complémentaires :</p>
<ul>
  <li>affinage discrétisation</li>
  <li>vaporisation</li>
  <li>sorties pour le démarrage à chaud</li>
</ul>
<h3>Affinage de la discrétisation (PRO)</h3>
<p>Cette option permet de limiter la distance maximale entre deux points de calcul à 20m.</p>
<p>Cette option est utilisée par défaut en transitoire rapide.&nbsp;</p>
<p>Elle est également conseillée pour les calculs de qualité.&nbsp;</p>
<p>A noter : si la simulation en transitoire rapide ou avec qualité est faite en démarrage à chaud (voir ci-dessous) à partir d'une autre simulation, il faut que cette dernière ait été simulée avec l'option.</p>
<h3>Vaporisation (PRO)</h3>
<p>Cette option modifie les calculs dans les cas de fortes dépressions. Avec cette option, les dépressions sont limitées à -10m. Sans cette option, les dépressions peuvent passer sous les -10m, ce qui n'est physiquement pas possible mais peut être utile pour visualiser l'ampleur des désordres de pression sur le réseau.</p>
<p>Cette option est surtout utile en transitoire rapide.</p>
<h3>Sorties pour le démarrage à chaud</h3>
<blockquote>
  <p>On appelle démarrage à chaud le fait d'utiliser un instant d'une simulation comme conditions initiales à une autre simulation.</p>
</blockquote>
<p>Cette option permet de définir l'instant à sauvegarder pour le démarrage à chaud par une autre simulation.&nbsp;</p>
<p>Attention, il ne faut pas faire cette sortie pour le démarrage à chaud au tout dernier pas de temps de calcul. Si vous voulez sauvegarder les résultats au bout de 24h, il est nécessaire de lancer la simulation sur une durée strictement supérieure à 24h.</p>
<p>Par ailleurs, les configurations sont à utiliser avec précaution en cas de démarrage à chaud : en effet, les seules configurations appliquées sont les configurations du scénario d'initialisation. On ne peut pas faire un démarrage à chaud avec une ou des configurations différentes de celles du scénario d'initialisation.</p>
<h2><strong>Choix du scénario de consommation</strong></h2>
<p>Permet de choisir le scénario de consommation à utiliser pour la simulation. Par défaut c'est le scénario de consommation de référence (WD_SCN_REF).</p>
<h2><strong>Paramétrage des conditions initiales</strong></h2>
<p>Deux possibilités sont disponibles :</p>
<ul>
  <li><strong>Départ au repos</strong> : Durée de simulation des conditions initiales.</li>
</ul>
<p>Dans ce cas le calcul est lancé pendant la durée indiquée avant la date de début de calcul pour mettre en eau le modèle. Pendant cette durée de mise en eau, les résultats de la simulation ne sont pas écrits dans les fichiers de sorties.</p>
<ul>
  <li><strong>Démarrage à chaud </strong>: Scénario de démarrage à chaud + temps de démarrage à chaud.</li>
</ul>
<p>Dans ce cas le calcul repart de l'instant sélectionné pour la sortie du démarrage à chaud du scénario sélectionné.</p>
<blockquote>
  <p>Il faut être vigilent dans le paramétrage des scénarios pour que le démarrage à chaud soit bien pris en compte. Le schéma suivant résume comment doivent se paramètrent les scénarios, notamment en termes de date de début et durée du calcul.</p>
  <figure class="image image_resized" style="width:49.33%;"><img src="/synoptique_dem_chaud.png">
    <figcaption>Synoptique du démarrage à chaud</figcaption>
  </figure>
</blockquote>
<h2><strong>Paramétrage des&nbsp;sorties</strong></h2>
<p>Le pas de temps de sortie est spécifié ici, ainsi que l'intervalle de sauvegarde des résultats.</p>
<h2><strong>Scenario de référence</strong></h2>
<p>Cette option permet de ne pas générer les fichiers nécessaires au moteur de calcul pour effectuer la simulation mais d'utiliser les fichiers générés pour le scénario de référence.</p>
<p>Dans ce cas, il faut que les scénarios aient tous les deux :</p>
<ul>
  <li>le même scénario de consommation</li>
  <li>les mêmes configurations sélectionnées</li>
</ul>
<h1>Ordre des modèles</h1>
<p>Il existe 3 méthodes de connexion des modèles :</p>
<h2>Globale</h2>
<p>Dans cette méthode, tous les modèles sont simulés en même temps. Si certains modèles ont des connecteurs de modèles ayant le même nom, ces modèles sont connectés.</p>
<h2>En cascade</h2>
<p>Dans cette méthode, tous les modèles sont simulés les uns après les autres, dans l'ordre spécifié par l'utilisateur. Pour modifier cet ordre, utiliser les flèches haut et bas à droite de la liste des modèles.</p>
<h2>Mixte</h2>
<p>Dans cette méthode, &nbsp;les modèles sont simulés par groupe, tel que spécifié par l'utilisateur.</p>
<p>Il est possible de créer ou de supprimer un groupe, avec + et - puis d'ajouter ou de supprimer des modèles dans les groupes avec les flèches.</p>
<blockquote>
  <p>Pour lancer une simulation sur un seul modèle, il faut choisir le mode mixte puis créer un groupe et y mettre le modèle que l'on veut simuler.</p>
</blockquote>
<h1>Configuration et régulation</h1>
<p>Il est possible de sélectionner une ou plusieurs configuration de réseau dans le scénario de calcul. Elles sont lues de bas en haut de sorte que si un même objet est configuré sur un même paramètres dans deux configurations appelées par le scénario, c'est la configuration du scénario du bas de la liste qui sera appliqué.</p>
<p>Attention, les configurations sont à utiliser avec précaution en cas de démarrage à chaud : en effet, les seules configurations appliquées sont les configurations du scénario d'initialisation. On ne peut pas faire un démarrage à chaud avec une ou des configurations différentes de celles du scénario d'initialisation (elles ne sont pas prises en compte).</p>
<h1>Options de calcul</h1>
<h2>Défaillances des raccords et défaillance des pompes</h2>
<p>Dans cet onglet, il est possible de paramétrer des fermeture brutales de vannes et des défaillances de pompes afin de simuler des phénomènes transitoires (coup de bélier).</p>
<h3>Fermeture brutale de vannes</h3>
<p>Les fermetures brutales de vannes nécessite d'avoir au préalable créer une courbe de fermeture (pour cela, ajouter une ligne à la table “courbe de défaillance” disponible dans le groupe “paramétrages” des couches QGIS).</p>
<p>Dans le menu du scénario, choisir le type de vanne, son nom, sa courbe de fermeture, l'heure de fermeture puis cliquer sur le <mark class="pen-green"><strong>+</strong></mark> pour ajouter la fermeture de la vanne.</p>
<h3>Défaillance de pompes</h3>
<p>Les défaillances de pompe sont modélisée selon 3 options possibles : une courbe automatique, une courbe 4 quadrants ou une courbe alpha(t) crée dans la table “courbe de défaillance” disponible dans le groupe “paramétrages” des couches QGIS).</p>
<p>Dans le menu du scénario, choisir la pompe, sa courbe d'arrêt, l'heure de la défaillance puis cliquer sur le <mark class="pen-green"><strong>+</strong></mark> pour ajouter la défaillance de la pompe.</p>
<h2>Fichier d'option</h2>
<p>Il est possible d'ajouter un fichier d'options pour des usages très particuliers. Pour disposer de la liste des options ou en créer des nouvelles pour vos besoins, merci de contacter le support.</p>
<h1>Qualité de l'eau</h1>
<p>Dans l'onglet qualité, deux options sont disponibles : Traceurs et Origine de l'eau. Ces deux options ne peuvent pas être simulée en même temps.</p>
<figure class="image image_resized" style="width:64.27%;"><img src="/scn_setting_quality.png"></figure>
<h2>Traceurs</h2>
<p>Il y a plusieurs options possibles :</p>
<ul>
  <li>Chlore</li>
  <li>THM</li>
  <li>Traceur passif</li>
  <li>Temps de séjour et temps de contact</li>
</ul>
<p>Pour plus de précision, consulter la page : <a href="https://xpresseau.hydra-software.net/fr/modelisation/qualit%C3%A9">https://xpresseau.hydra-software.net/fr/modelisation/qualit%C3%A9</a></p>
<h2>Origine de l'eau</h2>
<p>Cette option permet de quantifier la part d'eau de chaque source aux nœuds du réseau. Il est possible de suivre jusqu'à 8 sources.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
