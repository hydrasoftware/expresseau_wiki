<!--
title: Calculs hydrauliques
description: 
published: true
date: 2024-01-16T15:14:10.025Z
tags: 
editor: ckeditor
dateCreated: 2021-05-18T16:30:19.252Z
-->

<h1>Vérification topologique et prétraitement avant simulation</h1>
<p>Il peut arriver qu'en cours de construction d'un modèle, on veuille déconnecter une partie du réseau (soit en ne le rattachant pas au reste du modèle, soit en l'isolant par canalisations fermées, vannes fermées ou stabilisateur de pression aval avec pression de consigne nulle).</p>
<p>Ces portions de modèles, si elles n'ont pas de référence piézométrique, déstabilisent et ralentissent les calculs, voire les faussent.</p>
<h2>Vérification topologique des parties de réseau : couche “sans piezo”</h2>
<p>Express'eau effectue une vérification topologique des données du réseau :</p>
<ul>
  <li>Recherche des parties du réseau non rattachées au reste du réseau et sans référence piézométrique.</li>
  <li>Recherche des parties du réseau isolées (canalisations fermées, vannes fermées ou stabilisateur de pression aval avec pression de consigne nulle) du reste du réseau et sans référence piézométrique.</li>
</ul>
<p>Ces parties du réseau sont signalées au modélisateur dans la couche “sans piezo”.</p>
<h2>Prétraitement avant simulation</h2>
<p>Les parties du réseau présentes dans la couche “sans piezo” <u>ne sont pas envoyées au moteur de calcul.</u> Cela inclut les canalisations fermées, vannes fermées ou stabilisateur de pression aval qui sont responsables de la déconnexion d'une partie du réseau.</p>
<p>Attention : si des éléments responsables de la déconnexion d'une partie du réseau sont cités par le fichier de régulation (par exemple pour les ouvrir), il y aura un message d'erreur du moteur qui ne connaitra pas ces éléments. Il est donc primordial de les paramétrer comme “ouverts” dans la base pour conserver la partie du réseau dans la simulation et actionner les éléments en question.&nbsp;</p>
<p>&nbsp;</p>
<h1>Principe de la méthode de résolution</h1>
<h2>Discrétisation des équations</h2>
<h3><mark class="marker-blue">Sur les liaisons</mark></h3>
<p>Chaque liaison est régie par deux équations&nbsp;:</p>
<ul>
  <li>L’équation de continuité&nbsp;: F1 (Q, Z) = 0.</li>
  <li>L’équation de quantité de mouvement : F2 (Q, Z) = 0.</li>
</ul>
<p>(Q, Z) sont les variables inconnues de la liaison en un point courant de la liaison.</p>
<p>Chacune de ces équations est discrétisée suivant le schéma implicite de Preismann&nbsp;:</p>
<figure class="image"><img src="/discretisation.png"></figure>
<p>Où :</p>
<ul>
  <li>A&nbsp; est le point amont et C le point aval de l’élément hydraulique discrétisé.</li>
  <li>θ&nbsp;est compris entre 0,5 et 1</li>
</ul>
<p>L’algorithme de discrétisation est le suivant&nbsp;:</p>
<figure class="image"><img src="/discretisation_algo.png"></figure>
<p>Avec :</p>
<ul>
  <li>f<sub>A</sub>&nbsp; est la fonction évaluée au point A à un instant donné.</li>
  <li>f<sub>C</sub>&nbsp; est la fonction évaluée au point C à un instant donné.</li>
  <li>Δf<sub>A</sub> = f<sub>A</sub>(t+dt) - f<sub>A</sub>(t)</li>
  <li>Δf<sub>C</sub> = f<sub>C</sub>(t+dt) - f<sub>C</sub>(t)</li>
</ul>
<p>Après discrétisation du système d’équations entre les points extrémités amont A et aval C de la liaison, on obtient un système linéarisé &nbsp;d’ordre 1 sous la forme matricielle suivante&nbsp; pour chaque liaison :</p>
<figure class="image"><img src="/discretisation_matrice.png"></figure>
<p>Où :</p>
<ul>
  <li>les coefficients de la matrice M et du vecteur f sont calculés au temps t et&nbsp;:</li>
</ul>
<figure class="image"><img src="/image.png"></figure>
<ul>
  <li>Q<sub>A</sub>(t)&nbsp; est le débit entrant dans la liaison au point A (idem pour le point C),</li>
  <li>Z<sub>A</sub>(t)&nbsp; est la cote piézométrique au point A (idem pour le point C),</li>
  <li>dt&nbsp; est le pas de temps.</li>
</ul>
<p>&nbsp;</p>
<h3><mark class="marker-blue">Sur les singularités</mark></h3>
<p>Chaque singularité est quant à elle discrétisée sous la forme&nbsp;:</p>
<figure class="image"><img src="/eq2.png"></figure>
<p>où les coefficients <i>ra</i>, <i>sa</i> et <i>ta</i> sont évalués au temps t.</p>
<p>&nbsp;</p>
<h2>Assemblage des équations discrétisées</h2>
<p>Les variables &nbsp;de débit sont éliminées en écrivant que la somme algébrique des débits aboutissant à un nœud donné est nulle. On obtient ainsi un système matriciel global de la forme :</p>
<figure class="image"><img src="/eq3.png"></figure>
<p>Où Q<sub>i&nbsp;</sub>désigne un débit sortant (compté positivement) ou rentrant (compté négativement) d’une liaison connectée au nœud.</p>
<p>La substitution des équations (1) des liaisons dans les équations (2) fournit le système matriciel global suivant&nbsp;:</p>
<figure class="image"><img src="/eq4.png"></figure>
<p>&nbsp;</p>
<p>&nbsp;où&nbsp;:</p>
<ul>
  <li>K est une matrice carrée de rang N, N étant le nombre total de nœuds du modèle,</li>
  <li>f est un vecteur force de dimension N,</li>
  <li>dZ est la variation de cote piézométrique à chaque noeud entre deux pas de temps.</li>
</ul>
<p>Le système matriciel (3) est résolu à l’aide du&nbsp; solveur «&nbsp;Pardiso&nbsp;» réputé pour sa précision et sa très grande rapidité.</p>
<p>La connaissance du vecteur [dZ] permet de calculer les nouvelles cotes (Z) à chaque nœud au temps t<sub>n+1</sub>, puis les débits<sub>&nbsp; </sub>(Qa, Qc) à l’extrémité de chaque liaison à l’aide des équations (1) et (2).</p>
<p>Avec cette méthode l'équation de continuité aux nœuds est rigoureusement satisfaite à chaque itération.</p>
<h1>Adaptation de la méthode à chaque mode de calcul</h1>
<h2>Pour le calcul en régime établi</h2>
<p>La méthode de discrétisation décrite ci-dessus ne convient que si la distribution initiale (Q<sub>i</sub>,Z<sub>j</sub>) rentrée par l’utilisateur est proche de la solution exacte, ce qui en pratique est irréalisable puisque l’on ne connait pas la solution au démarrage des calculs. Le schéma de discrétisation de Preisman est dans ce cas remplacé par un schéma beaucoup plus robuste inspiré de la méthode de la sécante et qui converge vers le schéma de discrétisation de Preisman quand la solution calculée devient proche de la solution vraie.</p>
<p>La solution est obtenue par itération successives, chaque itération correspondant à un pas de temps fictif. Le nombre d’itérations requises pour approcher la solution en cote avec&nbsp; une tolérance de 1 cm sur les écarts entre deux itérations, est de l’ordre de&nbsp;5 pour les réseaux de quelques centaines de nœuds et 10 pour les réseaux de plusieurs milliers de nœuds&nbsp;: la convergence est donc très rapide.</p>
<h2>Pour le calcul en régime transitoire graduellement varié</h2>
<p>Le schéma de discrétisation de Preisman est appliqué entre deux pas de tems successifs, sauf lorsque les écarts en cotes sont trop importants. Dans ce cas le schéma de résolution devient itératif jusqu’à obtention de la convergence pour le pas de temps en cours. La&nbsp; gestion de la méthode est automatique dans le programme.</p>
<p>A noter que le mode 1 est précédé au premier pas de temps d’un calcul itératif d’équilibrage en mode 0, de façon à démarrer le calcul transitoire avec des conditions initiales équilibrées.</p>
<p>Si l’option de calcul de qualité est affichée le pas de discrétisation spatiale est ramené en interne à 20m (comme dans le mode 2 ci-après), de façon à&nbsp;limiter les effets de dispersion numérique du front de concentration.</p>
<h2>Pour le calcul en régime transitoire rapidement varié</h2>
<p>Deux différences importantes avec le mode ci-dessus&nbsp;:</p>
<ul>
  <li>Le pas de discrétisation le long d’un tronçon de canalisation est limité à 20 m&nbsp;; autrement dit le programme ajoute automatiquement des nœuds de calculs intermédiaires pour reproduire correctement la propagation des ondes de pression,</li>
  <li>Le programme détecte les modifications rapides de position d’organe. A chaque nouvelle détection il applique localement les équations des «&nbsp;caractéristiques&nbsp;» autour de l’organe &nbsp;et modifie la solution du pas de temps précédent en tenant compte des discontinuités générées par la manœuvre subite. La discontinuité est propagée ensuite à l’aide du schéma de discrétisation de Preisman.</li>
</ul>
<p>Supposons pour illustrer la méthode le cas d’une fermeture rapide de vanne en travers d’une canalisation&nbsp;:</p>
<figure class="image"><img src="/fermvanne.png"></figure>
<p>Les valeurs (QA, ZA, QB, ZB) sont modifiées par résolution du système local d’équations suivant&nbsp;:</p>
<p>&nbsp;</p>
<figure class="image"><img src="/eq_transitoire.png"></figure>
<p>C<sub>0</sub> est la vitesse de propagation de l’onde de pression et U la vitesse d’écoulement dans chaque conduite de part et d’autre de la vanne.</p>
<p>La fonction d’orifice f est exprimée avec la nouvelle section d’ouverture. La résolution locale de ce système a pour conséquence de générer un saut de pression (ZA’-ZA) et (ZB’-ZB) au point A et au point B respectivement, ce saut étant propagé aux pas de temps suivants par l’algorithme général de résolution implicite.&nbsp;</p>
<h3>Gestion des discontinuité</h3>
<p>Les coups de bélier (ou discontinuités de pression) sont générés&nbsp; par des manœuvres brusques d’organes ou par des changements d’états brutaux de fonctionnement. Le tableau ci-dessous récapitule les organes susceptibles de générer des coups de bélier et le mécanisme de génération associé.</p>
<p>&nbsp;</p>
<figure class="table" style="width:444.85pt;">
  <table style="border-bottom:none;border-left:none;border-right:none;border-top:none;">
    <tbody>
      <tr>
        <td style="border-bottom:1.0pt solid black;border-left:1.0pt solid black;border-right:1.0pt solid black;border-top:1.0pt solid black;padding:0cm 5.4pt;vertical-align:top;width:65.25pt;"><strong>Désignation&nbsp;</strong></td>
        <td style="border-bottom:1.0pt solid black;border-left:1.0pt none black;border-right:1.0pt solid black;border-top:1.0pt solid black;padding:0cm 5.4pt;vertical-align:top;width:159.9pt;"><strong>Organe&nbsp;</strong></td>
        <td style="border-bottom:1.0pt solid black;border-left:1.0pt none black;border-right:1.0pt solid black;border-top:1.0pt solid black;padding:0cm 5.4pt;vertical-align:top;width:219.7pt;"><strong>Mode de génération du coup de bélier</strong></td>
      </tr>
      <tr>
        <td style="border-bottom:1.0pt solid black;border-left:1.0pt solid black;border-right:1.0pt solid black;border-top:1.0pt none black;padding:0cm 5.4pt;vertical-align:top;width:65.25pt;">POMP</td>
        <td style="border-bottom:1.0pt solid black;border-left:none;border-right:1.0pt solid black;border-top:none;padding:0cm 5.4pt;vertical-align:top;width:159.9pt;">POMP</td>
        <td style="border-bottom:1.0pt solid black;border-left:none;border-right:1.0pt solid black;border-top:none;padding:0cm 5.4pt;vertical-align:top;width:219.7pt;">Rupture brutale d’alimentation électrique</td>
      </tr>
      <tr>
        <td style="border-bottom:1.0pt solid black;border-left:1.0pt solid black;border-right:1.0pt solid black;border-top:1.0pt none black;padding:0cm 5.4pt;vertical-align:top;width:65.25pt;">CLPT</td>
        <td style="border-bottom:1.0pt solid black;border-left:none;border-right:1.0pt solid black;border-top:none;padding:0cm 5.4pt;vertical-align:top;width:159.9pt;">Clapet</td>
        <td style="border-bottom:1.0pt solid black;border-left:none;border-right:1.0pt solid black;border-top:none;padding:0cm 5.4pt;vertical-align:top;width:219.7pt;">Fermeture brutale du clapet</td>
      </tr>
      <tr>
        <td style="border-bottom:1.0pt solid black;border-left:1.0pt solid black;border-right:1.0pt solid black;border-top:1.0pt none black;padding:0cm 5.4pt;vertical-align:top;width:65.25pt;">VANNE</td>
        <td style="border-bottom:1.0pt solid black;border-left:none;border-right:1.0pt solid black;border-top:none;padding:0cm 5.4pt;vertical-align:top;width:159.9pt;">Vanne plate</td>
        <td style="border-bottom:1.0pt solid black;border-left:none;border-right:1.0pt solid black;border-top:none;padding:0cm 5.4pt;vertical-align:top;width:219.7pt;">Fermeture brutale de la vanne</td>
      </tr>
      <tr>
        <td style="border-bottom:1.0pt solid black;border-left:1.0pt solid black;border-right:1.0pt solid black;border-top:1.0pt none black;padding:0cm 5.4pt;vertical-align:top;width:65.25pt;">REGQ</td>
        <td style="border-bottom:1.0pt solid black;border-left:none;border-right:1.0pt solid black;border-top:none;padding:0cm 5.4pt;vertical-align:top;width:159.9pt;">Vanne de régulation de débit</td>
        <td style="border-bottom:1.0pt solid black;border-left:none;border-right:1.0pt solid black;border-top:none;padding:0cm 5.4pt;vertical-align:top;width:219.7pt;">Fermeture brutale de la vanne</td>
      </tr>
      <tr>
        <td style="border-bottom:1.0pt solid black;border-left:1.0pt solid black;border-right:1.0pt solid black;border-top:1.0pt none black;padding:0cm 5.4pt;vertical-align:top;width:65.25pt;">REDP</td>
        <td style="border-bottom:1.0pt solid black;border-left:none;border-right:1.0pt solid black;border-top:none;padding:0cm 5.4pt;vertical-align:top;width:159.9pt;">Vanne de régulation de pression</td>
        <td style="border-bottom:1.0pt solid black;border-left:none;border-right:1.0pt solid black;border-top:none;padding:0cm 5.4pt;vertical-align:top;width:219.7pt;">Fermeture brutale de la vanne</td>
      </tr>
      <tr>
        <td style="border-bottom:1.0pt solid black;border-left:1.0pt solid black;border-right:1.0pt solid black;border-top:1.0pt none black;padding:0cm 5.4pt;vertical-align:top;width:65.25pt;">Q0</td>
        <td style="border-bottom:1.0pt solid black;border-left:none;border-right:1.0pt solid black;border-top:none;padding:0cm 5.4pt;vertical-align:top;width:159.9pt;">Prélèvement/injection de débit</td>
        <td style="border-bottom:1.0pt solid black;border-left:none;border-right:1.0pt solid black;border-top:none;padding:0cm 5.4pt;vertical-align:top;width:219.7pt;">Variation brutale du débit</td>
      </tr>
      <tr>
        <td style="border-bottom:1.0pt solid black;border-left:1.0pt solid black;border-right:1.0pt solid black;border-top:1.0pt none black;padding:0cm 5.4pt;vertical-align:top;width:65.25pt;">BALL</td>
        <td style="border-bottom:1.0pt solid black;border-left:none;border-right:1.0pt solid black;border-top:none;padding:0cm 5.4pt;vertical-align:top;width:159.9pt;">Ballon anti-bélier</td>
        <td style="border-bottom:1.0pt solid black;border-left:none;border-right:1.0pt solid black;border-top:none;padding:0cm 5.4pt;vertical-align:top;width:219.7pt;">Fin de vidange du ballon</td>
      </tr>
      <tr>
        <td style="border-bottom:1.0pt solid black;border-left:1.0pt solid black;border-right:1.0pt solid black;border-top:1.0pt none black;padding:0cm 5.4pt;vertical-align:top;width:65.25pt;">VNTS</td>
        <td style="border-bottom:1.0pt solid black;border-left:none;border-right:1.0pt solid black;border-top:none;padding:0cm 5.4pt;vertical-align:top;width:159.9pt;">ventouse</td>
        <td style="border-bottom:1.0pt solid black;border-left:none;border-right:1.0pt solid black;border-top:none;padding:0cm 5.4pt;vertical-align:top;width:219.7pt;">Fin d’évacuation de la poche d’air admise dans le réseau</td>
      </tr>
    </tbody>
  </table>
</figure>
<p>Le mécanisme générateur du coup de bélier dépend du type d’organe&nbsp;:</p>
<h4>Les vannes, les régulateurs</h4>
<p>Pour les vannes l’utilisateur doit fournir une courbe temporelle de décroissance du taux d’ouverture de la vanne. La courbe de décroissance a l’allure suivante&nbsp;:</p>
<figure class="image"><img src="/alps-vanne.png"></figure>
<p>La courbe de décroissance est discrétisée en forme en marches d’escalier, faisant apparaitre une série de sauts élémentaires. Le programme applique l’algorithme du paragraphe précédent à l’instant où le saut se produit, ce qui génère une petite discontinuité de pression à chaque borne de la vanne. Une nouvelle discontinuité de pression est générée à chaque nouvel instant où un saut de section d’ouverture est rencontré sur la courbe, l’accumulation de ces sauts est susceptible d’engendrer ou non un coup de bélier détectable selon les temps relatifs de fermeture de la vanne et de propagation des ondes dans le réseau.</p>
<h4>Les clapets</h4>
<p>Deux options sont disponibles&nbsp;: dans une option le programme détecte lui-même l’instant où la direction de l’écoulement s’inverse et applique alors la courbe de fermeture rentrée par l’utilisateur&nbsp;; cette fonction est définie par l’utilisateur en fonction des caractéristiques mécaniques du clapet et des conditions hydrauliques ambiantes.&nbsp;</p>
<p>Dans l’autre option l’utilisateur définit lui-même l’instant de démarrage de la fermeture s’inverse et applique alors une courbe de fermeture.</p>
<p>En pratique la première option est généralement appliquée.</p>
<blockquote>
  <p>A noter que si un objet Clapet est défini en aval d’une pompe il faut s’assurer que l’option «&nbsp;clapet&nbsp;» est désactivée dans l’élément Pompe.</p>
</blockquote>
<h4>Les pompes</h4>
<p>Deux options sont disponibles&nbsp;:&nbsp;</p>
<ul>
  <li>dans une option l’utilisateur rentre uniquement l’instant de la disjonction électrique. Le programme applique alors la courbe de décroissance de la vitesse de rotation de la pompe définie dans son paramétrage (voir <a href="/modelisation/pompe">ici</a>).</li>
  <li>Dans l’autre option l’utilisateur définit lui-même la courbe temporelle de décroissance de la vitesse de rotation après disjonction.</li>
</ul>
<p>&nbsp;</p>
<h4>Les débits injectés</h4>
<p>L’utilisateur définit une courbe temporelle de décroissance du débit initialement injecté. Chaque saut élémentaire de débit provoque une discontinuité de pression&nbsp;; on est ainsi ramené aux 2 cas ci-dessus.</p>
<h4>&nbsp;</h4>
<h4>Fin de vidange d’un ballon&nbsp; anti bélier, fermeture brutale d’un clapet anti retour</h4>
<p>Dans ce cas la discontinuité de fonctionnement&nbsp; n’est pas générée par une courbe prédéfinie mais elle est détectée par le programme à l’instant où un ballon en phase de vidange se vide totalement&nbsp;: le débit de vidange Q<sub>L</sub> juste avant la mise à sec du ballon devient brusquement nul, ce&nbsp; qui génère le coup de bélier. La détection et la gestion de cet événement possible au cours du calcul est totalement prise en charge par le programme. En pratique on dimensionne le volume du ballon pour éviter de provoquer ce type de phénomène qui peut être très destructeur.</p>
<p>L’instant de la fermeture totale d’un clapet anti retour génère également une surpression brutale, communément appelée «&nbsp;coup du clapet&nbsp;». Cette surpression est conditionnée par&nbsp; l’allure de la&nbsp; courbe de fermeture définie par l’utilisateur. Dans le cas d’une fermeture très rapide dès la détection d’une inversion d’écoulement aucune surpression n’est générée.</p>
<h4>Fin d’évacuation de la poche d’air par une ventouse&nbsp;</h4>
<p>Le mécanisme de génération du coup de bélier est analogue à celui du ballon&nbsp;: l’onde de pression provoque l’expulsion de la poche d’air créée par le passage antérieur d’une onde de dépression. Cette expulsion est vue par le réseau comme un soutirage d’eau avec un débit équivalent Q<sub>L</sub>. Au moment où la poche d’air se vide totalement le débit QL devient brusquement nul, ce qui génère un coup de bélier. Afin de minimiser ce phénomène également gênant, on sélectionne généralement une ventouse avec un diamètre d’orifice adapté par rapport au diamètre de la canalisation. Ce point est précisé<a href="/modelisation/ventouse"> ici</a>.&nbsp;</p>
<p>&nbsp;</p>
