<!--
title: Module de contrôle et de régulation avancée
description: 
published: true
date: 2025-01-13T12:11:15.871Z
tags: 
editor: ckeditor
dateCreated: 2021-05-18T15:48:32.398Z
-->

<h1 style="text-align:justify;">Introduction</h1>
<p style="text-align:justify;">Les ouvrages hydrauliques modélisés dans <strong>Express'eau</strong> sont paramétrés par défaut de façon à fonctionner à partir d'une régulation simple. Les paramètres par défaut sont définis dans la boite de dialogue des différents objets disponibles dans l’interface.&nbsp;</p>
<p style="text-align:justify;">La plupart des ouvrages sont fixes, d’autres sont mobiles et régulés par rapport à une consigne de pression ou de débit.&nbsp;</p>
<p style="text-align:justify;">Dans les situations réelles d’exploitation, ces ouvrages peuvent très bien être manœuvrés de façon différente de celle indiquée par la consigne par défaut, en réseau. On distingue deux cas :&nbsp;</p>
<ul>
  <li style="text-align:justify;">les états contraints : un ouvrage est consigné en raison d’un événement extérieur tel qu’une mise en chômage, un incident sur le réseau, etc.</li>
  <li style="text-align:justify;">gestion coordonnée : on peut être amené à moduler la consigne d’un ouvrage en fonction d’un algorithme de contrôle faisant intervenir tout un ensemble de paramètres d’état du système et d’actionneurs : la régulation simple n’est plus adaptée dans ce cas.</li>
</ul>
<p style="text-align:justify;">Afin de gérer ces situations, <strong>Express'eau </strong>dispose d'un module complet de <strong><u>régulation avancée</u></strong>, totalement interactif avec les algorithmes de calculs hydrauliques du noyau de calcul d'<strong>Express’Eau</strong>.</p>
<h1>Principe de la régulation et mise en œuvre</h1>
<h2>Paramétrage des régulations</h2>
<p>Concrètement, une stratégie de régulation avancée est totalement renseignée par un fichier ASCII qui contient une série d'instructions rentrées séquentiellement.</p>
<p>Pour être pris en compte dans la simulation, le fichier de régulation avancée doit être spécifié dans le paramétrage du scénario, via <code>Express'eau → Scénarios → Settings</code> puis dans l'onglet <code>“Regulation and configuration”</code> choisir le nom du bloc de régulation.</p>
<p>En l'absence de fichier&nbsp;de régulation avancée spécifié dans le scénario, le programme de calcul applique les algorithmes de contrôle simple définis dans le modèle hydraulique.</p>
<blockquote>
  <p>Pour l'édition du fichier ASCII de régulation, si vous travaillez sous Windows, il est préconisé de l'éditer avec l'outil Notepad++ (outil gratuit et open source) qui permet de formater de manière automatique le fichier de contrôle. Pour utiliser cette fonctionnalité, c'est ici : <a href="/Utilisation_Notepad++_fichiers_regulation">Utilisation de Notepad++ pour l'édition des fichiers de régulation</a>.&nbsp;</p>
</blockquote>
<h2>Objets concernés par la régulation avancée&nbsp;</h2>
<p>Seul un nombre limité de d'objets du modèle sont concernés par la régulation avancée. La liste suivante indique quels sont les objets “régulables” :</p>
<ul>
  <li>Les canalisations ("Pipe")</li>
  <li>Les vannes ("Valve")</li>
  <li>Les consommations ("User_Node")</li>
  <li>Les régulateurs de débit ("Flow Regulator")</li>
  <li>Les régulateurs de pression ("Pressure Regulator")</li>
  <li>Les clapets anti-retour ("Check Valve")</li>
  <li>Les pompes ("Pump")</li>
  <li>Les réservoirs ("Tank")</li>
</ul>
<p>Les paramètres sur lesquels la régulation avancée peut agir dépend de chaque élément. Ils sont précisés dans la suite de cette page.</p>
<p>Dans la suite de cette page, un ouvrage régulé via une régulation avancée est appelé “actionneur”.</p>
<h2>Description d'un bloc de régulation avancée</h2>
<p>Le bloc de régulation avancée se compose de groupes d'instructions exécutées en séquence à chaque pas de temps, dans l'ordre d'apparition des instructions.</p>
<p>On distingue :</p>
<ul>
  <li>les instructions d'affectation ou de calcul,</li>
  <li>les instructions de commande d'actionneurs,</li>
  <li>les instructions de rupture de séquence,</li>
  <li>les instructions de visualisation.</li>
</ul>
<h1>Instructions d'initialisation, d'affectation et de calcul (définition de variables)</h1>
<p>Les commandes DATA, SET et CALCUL permettent de définir des variables. On peut définir jusqu'à 200 variables</p>
<h2>La commande DATA</h2>
<p>Cette instruction sert à initialiser une valeur ou un tableau de valeurs dans une variable. La variable est initialisée dans la phase de lecture des données du fichier de contrôle. La commande est ignorée dans la phase calcul.</p>
<p>Elle s’écrit sur une seule ligne.</p>
<h3>a) Initialisation d’une variable simple&nbsp;</h3>
<p>DATA %vari = x ( x : valeur réelle)</p>
<h3>b) Initialisation d’un tableau de valeurs numériques réelles</h3>
<p>DATA %%tabi = x1 x2 … xn (xi : valeur réelle)</p>
<h3>c) Initialisation d’un tableau de chaines de caractères</h3>
<p>DATA &amp;&amp;ctabi = c1 c2 … cn</p>
<p>Le nombre de caractères de chaque chaine c1 c2 … cn doit être compris entre 1 et 24. Une chaine représente généralement un nom d’actionneur.</p>
<blockquote>
  <p><u>Astuce</u> : il est permis de modifier la valeur de la variable dans la suite du programme avec une commande SET. Cette double utilisation de DATA et SET permet d'initialiser la variable en début de calcul, puis de la modifier quand on le souhaite. Pratique par exemple pour simuler des pompes qui démarrent en alternance.</p>
</blockquote>
<h2>La commande SET</h2>
<p>Parmi les instructions disponibles, l'une revêt une importance particulière : il s'agit de la commande <code>SET</code> permettant de définir une variable.</p>
<h3>Syntaxe</h3>
<p>La définition d'une variable simple s'écrit:</p>
<p><code>SET %mavariable = VAR</code></p>
<p>« mavariable » est un libellé composé d'une chaîne de caractères alphanumérique ( 16 caractères au maximum).&nbsp;</p>
<p>« mavariable » désigne le nom d'une variable, visible dans l'ensemble des fichiers de régulation définis dans le scénario de calcul.</p>
<p>VAR est une valeur d'état pouvant être exprimée de 6 manières :</p>
<ul>
  <li>valeur numérique (ex : <code>%toto = 39.2</code>),</li>
  <li>autre variable (ex : <code>%1 = %tata</code>),</li>
  <li>paramètre hydraulique (ex : <code>%cc = ZAM NOD_AB</code>),</li>
</ul>
<p><span class="text-small"><i><u>Tableau n°2 : paramètres hydrauliques affectables à la définition d'une variable</u></i></span></p>
<figure class="table">
  <table>
    <tbody>
      <tr>
        <td>Type d'élément</td>
        <td>Paramètres hydrauliques disponibles</td>
      </tr>
      <tr>
        <td>
          <p style="text-align:center;">Noeud</p>
        </td>
        <td>
          <ul>
            <li>Q</li>
            <li>ZAM</li>
            <li>HAM</li>
            <li>ZAV</li>
            <li>HAV</li>
            <li>CL</li>
          </ul>
        </td>
      </tr>
      <tr>
        <td>
          <p style="text-align:center;">Liaison</p>
        </td>
        <td>
          <ul>
            <li>Q</li>
            <li>ZAM</li>
            <li>HAM</li>
            <li>ZAV</li>
            <li>HAV</li>
            <li>DH</li>
            <li>CL</li>
          </ul>
        </td>
      </tr>
    </tbody>
  </table>
</figure>
<ul>
  <li>position d'un actionneur (ex : <code>%1dd = VANNE ‘Van-32’</code>) Cette instruction permet d'agir sur les actionneurs en fonction de l'état d'autres actionneurs. Les valeurs renvoyées dans la variable dépendent du type de l'actionneur.</li>
</ul>
<p><span class="text-small"><i><u>Tableau n°2 : positions des actionneurs affectables à la définition d'une variable</u></i></span></p>
<figure class="table">
  <table>
    <tbody>
      <tr>
        <td>
          <p style="text-align:center;"><strong>ITYPE</strong></p>
        </td>
        <td>
          <p style="text-align:center;"><strong>PARAM correspondant</strong></p>
        </td>
      </tr>
      <tr>
        <td>
          <p style="text-align:center;">CANA</p>
        </td>
        <td>
          <p style="text-align:center;">STATUT</p>
        </td>
      </tr>
      <tr>
        <td>
          <p style="text-align:center;">VANNE</p>
        </td>
        <td>
          <p style="text-align:center;">ALPS</p>
        </td>
      </tr>
      <tr>
        <td>
          <p style="text-align:center;">CONSO</p>
        </td>
        <td>
          <p style="text-align:center;">ALPQ</p>
        </td>
      </tr>
      <tr>
        <td>
          <p style="text-align:center;">REGQ</p>
        </td>
        <td>
          <p style="text-align:center;">ALPS</p>
        </td>
      </tr>
      <tr>
        <td>
          <p style="text-align:center;">REDP</p>
        </td>
        <td>
          <p style="text-align:center;">ALPS</p>
        </td>
      </tr>
      <tr>
        <td>
          <p style="text-align:center;">POMP</p>
        </td>
        <td>
          <p style="text-align:center;">ALPR</p>
        </td>
      </tr>
      <tr>
        <td>
          <p style="text-align:center;">CHAT</p>
        </td>
        <td>
          <p style="text-align:center;">Niveau Z dans le réservoir</p>
        </td>
      </tr>
    </tbody>
  </table>
</figure>
<ul>
  <li>Variable temps : <code>TIME</code></li>
  <li>Variable pas de temps : <code>DT</code></li>
</ul>
<p>Il est également possible de définir une variable tableau.</p>
<p><code>SET %%tabi = x1 x2 … xn</code></p>
<p><code>%%tabi</code> désigne une variable tableau. Le nombre maximum de variables tableau différentes est 200.</p>
<p><code>xi </code>= valeurs numériques du tableau. On peut définir 20 valeurs au maximum.</p>
<h3>Exemple très utile</h3>
<p><code>set %TIME = TIME </code>permet de définir une variable %TIME qui prend la valeur du temps écoulé depuis le début de la simulation.</p>
<h2>La commande CALCUL</h2>
<p>Une autre instruction intéressante est l'instruction <code>CALCUL</code> qui porte sur des variables simples ou les variables tableaux.</p>
<h3>Calcul sur des variables simples</h3>
<p><code>CALCUL %vari = %varj (opérateur) %vark</code></p>
<p>&nbsp;où <code>opérateur</code> reconnaît les caractères : <code>+, -, .+, /.**</code></p>
<h3>Calcul sur des variables tableaux</h3>
<p><code>CALCUL %%tabi = #func ( arg1, arg2, ….., argn)</code></p>
<p>« func » est l'identificateur d‟une fonction accessible depuis la bibliothèque intégré au module de contrôle. Le nombre et le type des arguments dans la liste d‟appel depuis de la nature de la fonction : ce peuvent être des variables simples, des valeurs numériques ou des variables tableaux. Dans le cas d‟une variable tableau calculée la taille du tableau est déterminée par la nature de la fonction ou la liste d‟arguments.</p>
<h3>Calcul d'une variable simple à l'aide d'une fonction</h3>
<p><code>CALCUL %vari = #func ( arg1, arg2, ….., argn)</code></p>
<h3>Calcul d'une variable tableau à l'aide d'une fonction</h3>
<p><code>CALCUL %%tabi = #func ( arg1, arg2, ….., argn)</code></p>
<p>« <code>func</code> » est l’identificateur d’une fonction accessible depuis la bibliothèque intégré au module de contrôle. Le nombre et le type des arguments dans la liste d’appel depuis de la nature de la fonction : ce peuvent être des variables simples, des valeurs numériques ou des variables tableaux. Dans le cas d’une variable tableau calculée la taille du tableau est déterminée par la nature de la fonction ou la liste d’arguments.</p>
<p>Les fonctions implantées dans le module de contrôle sont définies à l’<strong>annexe 1</strong>.</p>
<blockquote>
  <p>Une fonction particulièrement intéressante permet de calculer le modulo par 24 et ainsi de créer une variable correspondant à l'heure de la journée :</p>
  <p><code>set %TIME = TIME</code></p>
  <p><code>CALCUL %heure = #TMOD24 ( %TIME )</code></p>
  <p>=&gt; Ces deux lignes ont pour effet que la variable %heure contienne l'heure de la journée (si la simulation démarre à 0h, bien-sûr, sinon il faut lui ajouter l'heure de début de simulation)</p>
</blockquote>
<p>Cette fonctionnalité est donc très puissante : toute nouvelle fonction nécessite néanmoins l’intervention du développeur pour son implantation.</p>
<p>NB : les lignes d’instruction SET et CALCUL ne doivent pas forcément être séparées pas des lignes blanches.</p>
<h1>Instructions d'activation d'un actionneur</h1>
<h2>Instructions conditionnelles</h2>
<p>Une instruction conditionnelle est construite de la façon suivante :</p>
<ul>
  <li>Sur la première ligne, on indique le type d'actionneur à réguler (<code>ITYPE</code>) et son identifiant (<code>ID</code>).</li>
  <li>Sur les lignes suivantes, on énumère les conditions d'activation et l'action à exécuter.</li>
</ul>
<p><span class="text-small"><i><u>Tableau n°2 : syntaxe d'une instruction conditionnelle&nbsp;</u></i></span></p>
<figure class="image"><img src="/bloc_instruction_condi.png"></figure>
<p><strong>Remarques :</strong></p>
<ul>
  <li>On peut séparer les mots clés (ou identificateurs) par au moins un et autant de blancs qu'on le souhaite, dans la limite dune longueur totale de 256 caractères par ligne.</li>
  <li>Les lignes d'instruction à l'intérieur d'un groupe doivent être contiguës (pas de lignes blanches).</li>
  <li>Lorsque que le modèle est généré à partir du logiciel <strong>Express'eau</strong>, l'identificateur de la singularité doit comporter le nom précédé du nom du réseau et du signe “<i>”. Exemple : “UDI9_V1</i>”.</li>
  <li>Un mot-clé ou un identificateur ne doit pas comporter de blancs.</li>
  <li>Les parenthèses entourant les instructions “cond1” et “cond2” ou “action” sont obligatoires.</li>
  <li>Le groupe entre crochets peut être omis. S'il est présent le mot clé AND ou OR est requis.</li>
  <li>S'il existe plusieurs lignes après la ligne 1, la séquence logique est interprétée comme suit :</li>
</ul>
<p><code>if ( ) and/or ( ) then ( )</code></p>
<p><code>endif</code></p>
<p><code>if ( ) and/or ( ) then ( )</code></p>
<p><code>endif</code></p>
<p>Un groupe peut donc contenir plusieurs lignes d'instruction. Le programme traite chaque ligne en séquence à chaque pas de temps de régulation.</p>
<p>L'action est activée si la condition logique ( cond 1 ) et/ou la condition logique ( cond2 )) est/sont satisfaite(s).</p>
<h4>Ligne 1 : Désignation de l'actionneur</h4>
<p>La tableau suivant liste les actionneurs régulables, leur ITYPE et les paramètres qui sont régulables.</p>
<p><span class="text-small"><i><u>Tableau n°3 : actionneurs régulables, ITYPE et paramètres régulables</u></i></span></p>
<figure class="table">
  <table>
    <tbody>
      <tr>
        <td>
          <p style="text-align:center;"><strong>Elément</strong></p>
        </td>
        <td>
          <p style="text-align:center;"><strong>Table</strong></p>
        </td>
        <td>
          <p style="text-align:center;"><strong>ITYPE</strong></p>
        </td>
        <td>
          <p style="text-align:center;"><strong>Paramètres régulables</strong></p>
        </td>
        <td>
          <p style="text-align:center;"><strong>Définition des paramètres</strong></p>
        </td>
      </tr>
      <tr>
        <td>
          <p style="text-align:center;">Canalisation</p>
        </td>
        <td>
          <p style="text-align:center;">Pipe</p>
        </td>
        <td>
          <p style="text-align:center;">CANA</p>
        </td>
        <td>
          <p style="text-align:center;">STATUT</p>
        </td>
        <td>
          <p style="text-align:center;">0 si fermée, 1 si ouverte</p>
        </td>
      </tr>
      <tr>
        <td>
          <p style="text-align:center;">Vanne</p>
        </td>
        <td>
          <p style="text-align:center;">Valve</p>
        </td>
        <td>
          <p style="text-align:center;">VANNE</p>
        </td>
        <td>
          <p style="text-align:center;">ALPS</p>
        </td>
        <td>
          <p style="text-align:center;">Fraction d'ouverture de la vanne (entre 0 et 1)</p>
        </td>
      </tr>
      <tr>
        <td>
          <p style="text-align:center;">Consommation</p>
        </td>
        <td>
          <p style="text-align:center;">User_Node</p>
        </td>
        <td>
          <p style="text-align:center;">CONSO</p>
        </td>
        <td>
          <p style="text-align:center;">ALPQ</p>
        </td>
        <td>
          <p style="text-align:center;">Fraction du débit nominal (entre 0 et 1)</p>
        </td>
      </tr>
      <tr>
        <td>
          <p style="text-align:center;">Piézométrie imposée</p>
        </td>
        <td>
          <p style="text-align:center;">Imposed piezometry</p>
        </td>
        <td>
          <p style="text-align:center;">ZO</p>
        </td>
        <td>
          <p style="text-align:center;">ZO</p>
        </td>
        <td>
          <p style="text-align:center;">Cote piézométrique (mNGF)</p>
        </td>
      </tr>
      <tr>
        <td>
          <p style="text-align:center;">Régulateur de débit</p>
        </td>
        <td>
          <p style="text-align:center;">Flow Regulator</p>
        </td>
        <td>
          <p style="text-align:center;">REGQ</p>
        </td>
        <td>
          <p style="text-align:center;">QCONS</p>
          <p style="text-align:center;">ALPS</p>
        </td>
        <td>
          <p style="text-align:center;">Débit de consigne en m3/h</p>
          <p style="text-align:center;">Fraction d'ouverture de la vanne (entre 0 et 1)</p>
        </td>
      </tr>
      <tr>
        <td>
          <p style="text-align:center;">Régulateur de pression</p>
        </td>
        <td>
          <p style="text-align:center;">Pressure Regulator</p>
        </td>
        <td>
          <p style="text-align:center;">REGP</p>
        </td>
        <td>
          <p style="text-align:center;">HCONS</p>
          <p style="text-align:center;">ALPS</p>
        </td>
        <td>
          <p style="text-align:center;">Pression de consigne</p>
          <p style="text-align:center;">Fraction d'ouverture de la vanne (entre 0 et 1)</p>
        </td>
      </tr>
      <tr>
        <td>
          <p style="text-align:center;">Clapet anti-retour</p>
        </td>
        <td>
          <p style="text-align:center;">Check Valve</p>
        </td>
        <td>
          <p style="text-align:center;">CLPT</p>
        </td>
        <td>
          <p style="text-align:center;">ALPS</p>
        </td>
        <td>
          <p style="text-align:center;">Fraction d'ouverture du clapet (entre 0 et 1)</p>
        </td>
      </tr>
      <tr>
        <td>
          <p style="text-align:center;">Pompe</p>
        </td>
        <td>
          <p style="text-align:center;">Pump</p>
        </td>
        <td>
          <p style="text-align:center;">POMP</p>
        </td>
        <td>
          <p style="text-align:center;">QCONS</p>
          <p style="text-align:center;">HCONS</p>
          <p style="text-align:center;">ALPR</p>
        </td>
        <td>
          <p style="text-align:center;">Débit de consigne en m3/h</p>
          <p style="text-align:center;">Pression de consigne au refoulement en m</p>
          <p style="text-align:center;">Fraction de la vitesse de rotation de la pompe</p>
        </td>
      </tr>
      <tr>
        <td>
          <p style="text-align:center;">Réservoir</p>
        </td>
        <td>
          <p style="text-align:center;">Tank</p>
        </td>
        <td>
          <p style="text-align:center;">CHAT</p>
        </td>
        <td>
          <p style="text-align:center;">ALPS1</p>
          <p style="text-align:center;">ALPS2</p>
          <p style="text-align:center;">ZCHAT</p>
        </td>
        <td>
          <p style="text-align:center;">Fraction d'ouverture du clapet d'alim. (entre 0 et 1)</p>
          <p style="text-align:center;">Fraction d'ouverture du clapet de vidange (entre 0 et 1)</p>
          <p style="text-align:center;">Niveau d'eau dans le réservoir</p>
        </td>
      </tr>
    </tbody>
  </table>
</figure>
<p>Commentaire :</p>
<ul>
  <li>Le paramètre « <code>QINC</code> » permet de superposer à un point de consommation donné un débit supplémentaire correspondant généralement à un soutirage d’une borne incendie. Un scénario « incendie » peut ainsi être facilement simulé via cette commande (cf. 3.5 ci-après)</li>
  <li>Le paramètre « <code>ZCHAT</code> » permet de spécifier le maintien d’un niveau d’eau donné dans un réservoir pendant une plage de temps par exemple. Cette instruction est spécialement utile dans le cas où l’on souhaite imposer un niveau initial donné dans un réservoir à l’instant de démarrage effectif de la simulation (généralement t=0). Les temps négatifs servent dans ce cas à atteindre un régime d’équilibre du modèle tout en imposant le niveau d’eau souhaité dans le réservoir.</li>
  <li>Le paramètre « <code>CLREG </code>» permet de moduler la concentration « objectif » en un point de consigne. Cette concentration est régulée par le module de régulation implanté dans l’élément CONS0.</li>
  <li>Le paramètre « <code>CLCONC</code> » permet d’imposer un flux d’injection de chlore en définissant la concentration associée au débit d’injection.</li>
</ul>
<p>A noter que :</p>
<ul>
  <li>La commande <code>CLREG</code> n’est acceptée que si le point d’injection en défini en mode régulée.</li>
  <li>La commande <code>CLCONC</code> n’est acceptée que si le point d’injection en défini en mode non régulé.</li>
  <li>Les 2 commandes <code>CLREG</code> et <code>CLCONC</code> restent effectives dans le mode de calcul découplé de qualité découplé.</li>
</ul>
<p>&nbsp;</p>
<h3>Lignes suivantes : instructions conditionnelles</h3>
<p>Une instruction conditionnelle est de la forme :</p>
<p><code>Si (COND1) et/ou (COND2) alors (ACTION)</code></p>
<p><strong>a - Syntaxe des conditions</strong></p>
<p>Les conditions <code>COND1</code> et <code>COND2</code> sont des comparaisons entre deux variables d'état, de la forme : <code>VAR-A GT/LT VAR-B&nbsp;</code></p>
<p>où <code>VAR-A</code> et <code>VAR-B</code> peuvent être :</p>
<ul>
  <li>des valeurs numériques figurant explicitement dans la ligne de commande (attention à comparer des flottants entre eux et donc à mettre un “.” même en l'absence de décimale, par exemple “10.” et non “10”)</li>
  <li>des variables locales qui auront été déclarées par une instruction <code>SET</code> ou <code>CALCUL</code> (voir paragraphe idoine)</li>
  <li>des paramètres hydrauliques désignant une grandeur connue à l'instant t en un point du réseau (débit, cote, variation de débit ou de cote, etc.). La syntaxe est détaillée ci-après.</li>
  <li>La variable temps <code>TIME</code> exprimée en heures relatives à la date de début de simulation</li>
  <li>le dernier pas de temps de calcul <code>DT</code> exprimé en heures.</li>
</ul>
<p>et où :</p>
<ul>
  <li><code>GT</code> signifie “supérieur”</li>
  <li><code>LT</code> signifie “inférieur”</li>
  <li><code>GE</code> signifie “supérieur ou égal”</li>
  <li><code>LE</code> signifie “inférieur ou égal”</li>
  <li><code>EQ</code> signifie “égal”</li>
  <li><code>NE</code> signifie “différent”</li>
</ul>
<h5><span style="font-family:'Trebuchet MS', Helvetica, sans-serif;">Syntaxe des paramètres hydrauliques :</span></h5>
<p>Ce paramètre est exprimé par la juxtaposition de deux champs comme suit :</p>
<p><span class="text-small"><i><u>Tableau n°4 :&nbsp;paramètres hydrauliques existant et syntaxes</u></i></span></p>
<figure class="table">
  <table>
    <tbody>
      <tr>
        <td>Q</td>
        <td rowspan="6">
          <p>+ ID du point de contrôle&nbsp;</p>
          <p>(nœuds, singularité ou liaison)</p>
          <p>sans ‘quotes’</p>
        </td>
      </tr>
      <tr>
        <td>ZAM</td>
      </tr>
      <tr>
        <td>ZAV</td>
      </tr>
      <tr>
        <td>HAM</td>
      </tr>
      <tr>
        <td>HAV</td>
      </tr>
      <tr>
        <td>DH</td>
      </tr>
    </tbody>
  </table>
</figure>
<p>Les règles de cohérence suivantes s'appliquent, selon la grandeur invoquée :</p>
<p><span class="text-small"><i><u>Tableau n°5 :&nbsp;paramètres hydrauliques existant, objets compatibles et signification</u></i></span></p>
<figure class="table">
  <table>
    <tbody>
      <tr>
        <td><mark class="pen-red">si grandeur = Q</mark></td>
        <td><mark class="pen-red">Le point de contrôle est nécessairement une liaison latérale : QLIAIS est alors le débit passant par la liaison.</mark></td>
      </tr>
      <tr>
        <td><mark class="pen-red">si grandeur =&nbsp;ZAM</mark></td>
        <td rowspan="4">
          <p><mark class="pen-red">Le point de contrôle est nécessairement une extrémité d'une liaison ou l'amont ou l'aval immédiat d'une singularité.</mark></p>
          <p><mark class="pen-red">ZAM : cote du nœud amont de la liaison ou cote en amont immédiat singularité</mark></p>
          <p><mark class="pen-red">ZAV : cote du nœud aval de la liaison ou cote en aval immédiat singularité</mark></p>
          <p><mark class="pen-red">HAM: pression statique au nœud amont ou pression statique en amont immédiat singularité</mark></p>
          <p><mark class="pen-red">HAV: pression statique au nœud aval ou pression statique en aval immédiat singularité</mark></p>
        </td>
      </tr>
      <tr>
        <td><mark class="pen-red">si grandeur =&nbsp;ZAV</mark></td>
      </tr>
      <tr>
        <td><mark class="pen-red">si grandeur =&nbsp;HAM</mark></td>
      </tr>
      <tr>
        <td><mark class="pen-red">si grandeur =&nbsp;HAV</mark></td>
      </tr>
      <tr>
        <td><mark class="pen-red">si grandeur =&nbsp;DH</mark></td>
        <td><mark class="pen-red">perte de pression entre le nœud amont et le nœud aval d'une liaison</mark></td>
      </tr>
    </tbody>
  </table>
</figure>
<p><strong>b - Syntaxe d'une instruction (action)</strong></p>
<p><code>ACTION</code> est une commande de la forme :<code>PARAM VAR-Action</code></p>
<p>où <code>PARAM</code> est un des paramètres qu'il est possible de réguler pour l'actionneur de type <code>ITYPE</code>. ces possibilités dépendent du type d'élément et sont présentées dans le <i><u>tableau n°1</u></i> de la présente page.</p>
<p>L'instruction couvre les cas possibles suivants :</p>
<ul>
  <li>pilotage d'une consigne en débit ou en cote</li>
  <li>déplacement d'actionneur</li>
  <li>consignation d'un actionneur régulable en position fixe</li>
</ul>
<p><code>VAR-Action</code> peut être :</p>
<ul>
  <li>une valeur numérique (attention pour les flottants mettre un “.” même en l'absence de décimale, par exemple “10.” et non “10”)</li>
  <li>une variable locale exprimée sous la forme : %mavariable</li>
  <li>le mot clé <code>DEFAUT</code> : cette déclaration a pour effet d'activer le mode de pilotage par défaut déclaré dans le modèle de base, ce qui est pratique dans le cas d'un état contraint : en fin de séquence d'état contraint, le programme va automatiquement réactiver le pilotage initial en vigueur, sans que l'opérateur n'ait besoin de se rappeler les paramètres associés.</li>
</ul>
<p><strong><u>c - Exemple</u></strong></p>
<pre><code class="language-plaintext">
POMP '4_P_Cesson'
IF ( TIME LT 0 ) THEN ( ALPR 1. ) ENDIF
IF ( TIME GT 0 ) AND ( %nivRPere GT 113.7 ) THEN ( ALPR 0. ) ENDIF
IF ( TIME GT 0 ) AND ( %nivRPere LT 113.05 ) THEN ( ALPR 1. ) ENDIF

REGQ '4_REGQ_1'
IF ( TIME GT 0 ) THEN ( QCONS 0. ) ENDIF
IF ( TIME GT 4 ) THEN ( QCONS 28. ) ENDIF
IF ( TIME GT 6.5 ) THEN ( QCONS 0. ) ENDIF</code></pre>
<p>&nbsp;</p>
<h2>Instructions non conditionnelles</h2>
<p>L'instruction : <code>IF ( cond1 ) [AND / OR] [( cond2 )] THEN ( action )</code> peut tout à fait être remplacée par l'instruction : <code>action</code> .</p>
<p>Dans ce cas l'instruction est exécutée sans condition.</p>
<p>NB : ne pas entourer cette instruction de parenthèses s'il n'existe pas de condition.</p>
<h1>Instructions de commentaires et de fin de programme</h1>
<p>Afin d’augmenter la lisibilité du fichier de régulation, on peut introduire le signe « ! » sur n’importe qu’elle ligne : les caractères de la ligne inscrits à droite de ce signe sont ignorés.</p>
<p><s>Par ailleurs, le programme s’arrête de lire le fichier lorsqu’il rencontre une ligne commençant par les caractères « - - - - -» (un caractère « -» suffit). La commande FIN provoque le même résultat.</s> Pour le moment, le fichier est lu dans son intégralité.&nbsp;</p>
<h1>Instructions de débogage de la régulation</h1>
<p>Ces deux instructions ont pour objet de faciliter le traçage des algorithmes de contrôle en phase de mise au point.</p>
<p>L’instruction <code>DEBUG</code> n’apparaît en principe qu’une seule fois dans le fichier de contrôle, avec la syntaxe suivante :</p>
<p><code>DEBUG tdeb tfin dt_debug</code></p>
<p>Les trois paramètres sont des valeurs numériques exprimées en heures :</p>
<ul>
  <li><code>tdeb</code> est la date de début de l’affichage des résultats dans la phase de régime transitoire,</li>
  <li><code>tfin</code> est la date de fin de l’affichage des résultats dans la phase de régime transitoire,</li>
  <li><code>dt_debug</code> est la durée de simulation séparant deux affichages.</li>
</ul>
<p>L’instruction <code>DEBUG</code> :</p>
<ul>
  <li>provoque l’écriture des lignes d’instructions du fichier *.REG au fur et à mesure de leur lecture en phase de lecture,</li>
  <li>provoque l’affichage en phase transitoire des valeurs prises par les variables locales déclarées par l’instruction SHOW.</li>
</ul>
<p>L’instruction <code>SHOW</code> est définie comme suit :</p>
<p><code>SHOW %i ‘commentaire’</code></p>
<p>Lors de l’exécution la valeur prise par la variable %i sera affichée tous les <code>dt_debug</code> pas de temps, accompagnée du commentaire contenu dans « commentaire » et du temps de calcul. Une pause est introduite après chaque pas de temps d’affichage.</p>
<h1>Métacommandes</h1>
<p>Une métacommande est une instruction affectant le mode de lecture des fichiers de contrôle.</p>
<p>Une métacommande est identifiée par la présence du caractère « $ » en tête de ligne : ce caractère peut être placé dans une colonne quelconque mais ce doit être le premier caractère de la ligne.</p>
<p>Les métacommandes reconnues dans la version actuelle sont :</p>
<ul>
  <li>« $ » : SELECT ivalue</li>
  <li>« $ » : CASE (ival1; iavl2 ; ival11, …)</li>
  <li>« $ » : END SELECT</li>
  <li>« $ » : QINC</li>
  <li>« $ » : Z0</li>
</ul>
<h3>Commentaires concernant la méta-commande « SELECT »</h3>
<p>Un groupe de commande est constitué par :</p>
<p>- une commande SELECT</p>
<p>- une ou plusieurs commandes CASE</p>
<p>- une commande END SELECT</p>
<p>Une commande CASE est suivie d’un groupe quelconque d’instruction. Ce groupe est là uniquement si une des valeurs affichées dans la ligne de commande est égale à <code>ivalue</code>.</p>
<p><code>ivalue</code> : nombre entier</p>
<p><code>ivali</code> : nombres entiers délimités par le séparateur « ; » (n ≤ 20)</p>
<p><u>Exemple</u></p>
<pre><code class="language-plaintext">$ SELECT (2)
$ CASE (0 ; 1)
{ groupe 1 d’instruction }
$ CASE (0 ; 2)
{ groupe 2 d’instructions }
$ END SELECT</code></pre>
<p>Dans cet exemple, seul le groupe 2 d’instructions sera lu, les instructions du groupe 1 sont ignorées.</p>
<p>On peut définir autant de groupes qu’on le désire.</p>
<p><u>Formulation alternative :</u></p>
<p>Le programme reconnaît également la liste d’instructions suivantes :</p>
<pre><code class="language-plaintext">« $ » : SELECT
« $ » : CASE (Cres1; Cres2 ; Cres3, …)
« $ » : END SELECT</code></pre>
<p>Les différences avec la formulation précédente sont les suivantes :</p>
<ul>
  <li>aucun paramètre déclaré après l’instruction <code>SELECT</code></li>
  <li>la liste déclarée par l’instruction CASE est constituée de variable caractères.</li>
</ul>
<p>Le groupe d’instructions à la suite de la metacommande <code>CASE </code>est exécuté si un des paramètres de la liste correspond à un des noms de réseaux listés dans le fichier <code>SCENARIO.NOM</code>. Ce fichier est produit par <strong>Express'eau </strong>au début de chaque nouvelle simulation.</p>
<p>Cette procédure alternative permet de conserver les mêmes structures de fichiers de commande , que l’on travaille en mode « Réseau » ou en mode « Complexe ».</p>
<h3>Commentaires concernant la méta commande « QINC »</h3>
<p>Cette métacommande est utilisée dans le cas particulier d’un scénario d’incendie. Elle permet de générer facilement une série d’instructions simulant en une seule fois tous les scénarios de soutirage correspondant à diverses configurations d’incendie.</p>
<p>Il est obligatoire que la première instruction lue dans le fichier soit : « <code>$ QINC</code> »</p>
<p>La structure du fichier est définie au format « CSV ». Le fichier se compose :</p>
<ul>
  <li>De lignes de commentaires. Une ligne de commentaire doit commencer obligatoirement par le caractère « ! » en colonne 1.</li>
  <li>De lignes de données, chaque ligne contenant les 5 champs définis dans l’ordre ci-dessus.</li>
</ul>
<p>Une ligne de commentaire est utile pour préciser les 5 champs renseignés aux lignes suivantes : « <code>Tdeb ; Tfin ; Cid_reseau ; Cid_el ; Qinc ;</code> ». Les noms des champs peuvent être quelconques. Par contre l’ordre dans lequel sont rentrés les paramètres doit être respecté comme suit :</p>
<ul>
  <li><code>Tdeb – Tfin </code>: intervalle de temps relatif pendant lequel le soutirage Qinc est appliqué,</li>
  <li><code>Cid_el </code>: nom de l’élément ; il est obligatoirement de type Q0 ou Qv,</li>
  <li><code>Cid_reseau </code>: nom du réseau auquel appartient l’élément.</li>
  <li><code>Qinc</code> : débit soutiré sur l’intervalle de temps <code>[Tdeb Tfin]</code> , en m3/h. en dehors de cet intervalle ce débit est nul. Il s’ajoute au débit de prélèvement normal défini dans l’élément.</li>
</ul>
<p>Les lignes blanches entre deux lignes d’instructions sont admises. Elles sont ignorées par le programme.</p>
<p>Cette métacommande permet également de définir un hydrogramme de soutirage ou d’injection au droit d’un point de consommation. Il convient dans ce cas d’utiliser un élément Q0 et de définir par défaut q0=0. En cas d’injection le débit est négatif. Cette fonctionnalité est utile dans l’étape d’un calage d’un modèle.</p>
<p>En pratique la structure d’un tel fichier est détectée lors de l’exécution du programme <i><u>Xeau.exe</u></i>. Chaque ligne est alors transformée dans un format de lecture compris par la routine de décodage du module de contrôle.</p>
<h3>Commentaires concernant la méta commande « Z0 »</h3>
<p>Cette métacommande permet d’imposer une courbe d’évolution temporelle d’une pression à un nœud.</p>
<p>Il est obligatoire que la première instruction lue dans le fichier soit : « <code>$ Z0</code> »</p>
<p>La structure du fichier est définie au format « *.CSV ». Le fichier se compose :</p>
<ul>
  <li>De lignes de commentaires. Une ligne de commentaire doit commencer obligatoirement par le caractère « ! » en colonne 1.</li>
  <li>De lignes de données, chaque ligne contenant les 5 champs définis dans l’ordre ci-dessus.</li>
</ul>
<p>Une ligne de commentaire est utile pour préciser les 5 champs renseignés aux lignes suivantes : « <code>Tdeb ; Tfin ; Cid_reseau ; Cid_el ; Z0 ;</code> ». Les noms des champs peuvent être quelconques. Par contre l’ordre dans lequel sont rentrés les paramètres doit être respecté comme suit :</p>
<ul>
  <li><code>Tdeb – Tfin</code> : intervalle de temps relatif pendant lequel le soutirage Qinc est appliqué,</li>
  <li><code>Cid_el </code>: nom de l’élément ; il est obligatoirement de type Z0,</li>
  <li><code>Cid_reseau</code> : nom du réseau auquel appartient l’élément.</li>
  <li><code>Z0</code> : cote piézo. imposée sur l’intervalle de temps <code>[Tdeb Tfin]</code> , en mNGF. En dehors de cet intervalle la cote retenus est celle définie par défaut dans l’élément Z0.</li>
</ul>
<p>Les lignes blanches entre deux lignes d’instructions sont admises. Elles sont ignorées par le programme.</p>
<p>Cette fonctionnalité est utile dans l’étape d’un calage d’un modèle.</p>
<p>En pratique la structure d’un tel fichier est détectée lors de l’exécution du programme <i><u>Xeau.exe</u></i>. Chaque ligne est alors transformée dans un format de lecture compris par la routine de décodage du module de contrôle.</p>
<h1>Exécution d'un algorithme de contrôle</h1>
<p>Si le fichier de contrôle est détecté, les tâches de calcul se déroulent comme suit :</p>
<h2><strong>Phase de calcul des données</strong></h2>
<p>Chaque instruction de contrôle est lue et compilée par le programme.</p>
<h2><strong>Phase d'exécution</strong></h2>
<p style="text-align:justify;">Au début de chaque pas de temps de calcul, le module de contrôle est appelé. Les instructions sont analysées en séquence et actualisées. Lorsqu'une instruction conditionnelle est vérifiée, le module de contrôle appelle la routine du module hydraulique concerné et celle-ci actualise le paramètre transmis par le module de contrôle.&nbsp;</p>
<p style="text-align:justify;">Tous les modules sont équipés de temporisation pour assurer des variations progressives d'état.</p>
<p>Deux remarques importantes :</p>
<ul>
  <li>les variables locales sont définies dans un «COMMON ». Elles ne sont donc pas effacées entre deux appels au module de contrôle,</li>
  <li>lors d'une procédure de démarrage à « chaud », le programme conserve la mémoire du mode de pilotage de chaque actionneur géré par le module de contrôle au cours de la simulation antérieure. Cette potentialité est mise à profit dans le cadre des simulations en continu.</li>
</ul>
<h1>Exemple de fichier de contrôle</h1>
<h2>Ex 1 : Pilotage d’une consigne de débit d’un module RGQ en fonction du temps</h2>
<ul>
  <li><u>Syntaxe :</u></li>
</ul>
<pre><code class="language-plaintext">REGQ ‘VAL2 _ ANTOB92_RQ2’
IF ( TIME LT 3.0 ) THEN ( QREG 0.5 )
IF ( TIME GT 3.0 ) THEN ( QREG 4.0 )
IF ( TIME GT 4.0 ) THEN ( QREG 1.0 )</code></pre>
<p>Avec ces instructions :</p>
<ul>
  <li>QREG = 0.5 si TIME &lt; 3.0</li>
  <li>QREG = 4.0 si 3.0 &lt; TIME &lt; 4.0</li>
  <li>QREG = 1.0 si TIME &gt; 4.0</li>
</ul>
<h2>Ex 2 : Utilisation de variable tableaux : fermeture d’une vanne en cas de dépassement de volume de remplissage de bassin</h2>
<p><u>Syntaxe :</u></p>
<pre><code class="language-plaintext">! définition de la courbe de remplissage
SET %%1 = 10 12 14 16 ! cote bassin
SET %%2 = 0 10000 15000 30000 ! volume bassin
SET %1 = Z NOD-BAS1
! calcul du volume de remplissage correspondant à la cote %1
CALCUL %2 = NTERP ( %1 %1%1 %%2 )
! fermeture de la vanne VAL1_VAN3 si volume de remplissage supérieur à 20000 m3
VA ‘VAL1_VAN3’
IF ( %2 GT 20000 ) THEN ( ALPS 0 )</code></pre>
<h2>Ex 3 : Utilisation des instructions IF THEN ENDIF</h2>
<p><u>Syntaxe :</u></p>
<pre><code class="language-plaintext">IF ( TIME LT 3.0 ) THEN

 RGQ ‘VALANTOB’
 QREGUL 0.5
  
ENDIF

IF ( TIME GT 3.0 ) THEN

  IF ( Z FRES57 LT 38.5 ) THEN
    RGQ ‘VALANTOB’
    QREGUL 0.5
  ENDIF

 IF ( Z FRE57 GT 40.0 ) THEN
   RGQ ‘VALANTOB’
   QREGUL 4.0
 ENDIF
 
ENDIF</code></pre>
<h1>Annexe</h1>
<h2>Annexe 1 : Fonctions disponibles dans la bibliothèque du module de contrôle</h2>
<figure class="image"><img src="/nterp.png"></figure>
<figure class="image"><img src="/datetotime.png"></figure>
<figure class="image"><img src="/minmaxmoy.png"></figure>
<figure class="image"><img src="/nterpc.png"></figure>
